This extends Drupal core's tabledrag functionality with some additional
features: move to top, move to bottom, and shuffle.

Once you enable the module, these additional features will be applied to all
tabledrag-enabled tables on your site.
